import org.junit.*;
import static org.junit.Assert.*;

public class CalculatorTest.java{

    @Test
    public void testAdd(){
	double n1 = 2.5;
	double n2 = 0;
	double expected = 2.5;
	double result = Calculator.add(n1, n2);
	assertEquals(expected, result, 1e-6);
    }

    @Test
    public void testSubtract(){
	double expectedResult = 1;
	assertEquals(expectedResult, Calculator.subtract(2, 1), 1e-6);
    }
}
